import 'fs'
import { existsSync, mkdirSync, readdirSync, readFileSync, writeFileSync } from 'fs'

export const mkSubDir = (username, subdir) => {
    if (!existsSync(`./data/${username}`)) {
        mkdirSync(`./data/${username}`)
    }
    if (!existsSync(`./data/${username}/${subdir}`)) {
        mkdirSync(`./data/${username}/${subdir}`)
    }
}

export const hasData = (username, dataFile) => {
    return existsSync(`./data/${username}/${dataFile}.json`)
}

export const saveData = (username, dataFile, object) => {
    if (!existsSync('./data')) {
        mkdirSync('./data')
    }
    if (!existsSync(`./data/${username}`)) {
        mkdirSync(`./data/${username}`)
    }
    // console.log(dataFile, object, JSON.stringify(object))
    writeFileSync(`./data/${username}/${dataFile}.json`, JSON.stringify(object))
}

export const loadData = (username, dataFile) => {
    let data = undefined;
    if (existsSync(`./data/${username}/${dataFile}.json`)) {
        data = JSON.parse(readFileSync(`./data/${username}/${dataFile}.json`))
    }
    return data;
} 

export const loadAllData = (username, subdir) => {
    let files = readdirSync(`./data/${username}/${subdir}`)
    let data = []
    for (let file of files) {
        let parsed = JSON.parse(readFileSync(`./data/${username}/${subdir}/${file}`))
        data.push(parsed)
    }
    return data
}
