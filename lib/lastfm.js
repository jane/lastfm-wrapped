import fetch from 'node-fetch'

const API_URL = "http://ws.audioscrobbler.com/2.0/"

export const callApi = async (method, params = {}) => {

    let url = `${API_URL}?method=${method}&api_key=${process.env.API_KEY}&format=json`
    if (params != undefined) {
        for (let [key, value] of Object.entries(params)) {
            url += `&${key}=${value}`
        }
    }
    let result = await fetch(url)
    return result.json();
}