import { callApi } from '../lib/lastfm.js'
import { hasData, mkSubDir, saveData } from '../lib/dataStore.js'

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

const retrieve = async (username, options) => {
    callApi("user.getinfo", { user: username }).then((data) => {
        saveData(username, "basic_info", data)
    })
    if (!options.basic) {
        await retrieveTracks(username, options)
        await retrieveAlbums(username, options)
        await retrieveArtists(username, options)
    }
}

const retrieveTracks = async (username, options) => {
    let period = '12month'
    if (options.full) {
        period = 'overall'
    }
    let pages = 0;
    mkSubDir(username, 'tracks')
    if (hasData(username, "tracks/tracks-1")) {
        console.log('already have tracks');
        return;
    }
    console.log('downloading page 1 of ???')
    let data = await callApi("user.getTopTracks", {
        user: username
    })
    pages = data.toptracks['@attr'].totalPages
    saveData(username, "tracks/tracks-1", data)
    for (let i = 2; i <= pages; i++) {
        await sleep(500)
        console.log(`downloading page ${i} of ${pages}`)
        let data = await callApi("user.getTopTracks", {
            user: username,
            page: i
        })
        saveData(username, `tracks/tracks-${i}`, data)
    }
}

const retrieveAlbums = async (username, options) => {
    let period = '12month'
    if (options.full) {
        period = 'overall'
    }
    let pages = 0;
    mkSubDir(username, 'albums')
    if (hasData(username, "albums/albums-1")) {
        console.log('already have albums');
        return;
    }
    console.log('downloading page 1 of ???')
    let data = await callApi("user.getTopAlbums", {
        user: username
    })
    pages = data.topalbums['@attr'].totalPages
    saveData(username, "albums/albums-1", data)
    for (let i = 2; i <= pages; i++) {
        await sleep(500)
        console.log(`downloading page ${i} of ${pages}`)
        let data = await callApi("user.getTopAlbums", {
            user: username,
            page: i
        })
        saveData(username, `albums/albums-${i}`, data)
    }
}

const retrieveArtists = async (username, options) => {
    let period = '12month'
    if (options.full) {
        period = 'overall'
    }
    let pages = 0;
    mkSubDir(username, 'artists')
    if (hasData(username, "artists/artists-1")) {
        console.log('already have artists');
        return;
    }
    console.log('downloading page 1 of ???')
    let data = await callApi("user.getTopArtists", {
        user: username
    })
    pages = data.topartists['@attr'].totalPages
    saveData(username, "artists/artists-1", data)
    for (let i = 2; i <= pages; i++) {
        await sleep(500)
        console.log(`downloading page ${i} of ${pages}`)
        let data = await callApi("user.getTopArtists", {
            user: username,
            page: i
        })
        saveData(username, `artists/artists-${i}`, data)
    }
}

export default retrieve