import { callApi } from "../lib/lastfm.js"

const api = (username, method, args, options) => {
    callApi(method, {
        username: username,
        ...JSON.parse(args)
    }).then((data) => {
        console.log(data)
    })
}

export default api