import { loadAllData, mkSubDir, saveData } from "../lib/dataStore.js"

const process = async (username, options) => {
    let data = {}
    let processed = {
        tracks: {},
        listenTime: {
            track: {},
            album: {},
            artist: {},
            sortedTracks: [],
            sortedAlbums: [],
            sortedArtists: []
        }
    }
    if (options.tracks) {
        data.tracks = loadAllData(username, 'tracks')

        // track-only data
        for (let page in data.tracks) {
            console.log(page, data.tracks[page])
            if (!data.tracks[page].toptracks) continue;
            let currentPage = data.tracks[page].toptracks.track
            // console.log(currentPage)
            for (let track of currentPage) {
                console.log('processing track', track.name)
                processed.tracks[track.mbid] = {
                    playcount: track.playcount,
                    name: track.name,
                    artist: track.artist.name
                }
                if (track.duration && parseInt(track.duration)) {
                    console.log('track has duration', track.duration)
                    let listenTime = parseInt(track.duration) * parseInt(track.playcount)
                    console.log('track listen time is', listenTime)
                    processed.listenTime.track[track.mbid] = {
                        seconds: listenTime,
                        minutes: listenTime / 60
                    }

                    console.log(track.mbid, processed.listenTime.track[track.mbid])
                    let key = ''
                    if (track.artist.mbid) { key = track.artist.mbid }
                    else { key = 'N-' + track.artist.name }

                    if (!processed.listenTime.artist[key]) {
                        processed.listenTime.artist[key] = {
                            seconds: listenTime,
                            minutes: listenTime / 60,
                            name: track.artist.name
                        }
                    }
                    else {
                        processed.listenTime.artist[key].seconds += listenTime
                        processed.listenTime.artist[key].minutes =
                            processed.listenTime.artist[key].seconds / 60
                    }
                }
                else {
                    processed.listenTime.track[track.mbid] = {
                        playcount: track.playcount
                    }
                }
            }
        }

        for (let track in processed.listenTime.tracks) {
            processed.listenTime.sortedTracks.push({
                mbid: track,
                ...processed.listenTime.track[track]
            })
        }

        processed.listenTime.sortedTracks.sort(
            (a, b) => {
                return b.seconds - a.seconds
            }
        )

        for (let artist in processed.listenTime.artist) {
            processed.listenTime.sortedArtists.push(
                {
                    mbid: artist,
                    ...processed.listenTime.artist[artist]
                }
            )
        }

        processed.listenTime.sortedArtists.sort(
            (a, b) => {
                return b.seconds - a.seconds
            }
        )
    }
    if (options.albums) {
        data.albums = loadAllData(username, 'albums')
        for (let page in data.albums) {
            let currentPage = data.albums[page].topalbums.album
        }
    }

    if (options.artists) {
        data.artists = loadAllData(username, 'artists')
        for (let page in data.artists) {
            let currentPage = data.artists[page].topartists.artist
        }
    }
    console.log('saving processed data')
    mkSubDir(username, 'processed')
    saveData(username, 'processed/gross', processed)
    saveData(username, 'processed/tracks', processed.tracks)
    saveData(username, 'processed/listenTime', processed.listenTime)
}

export default process