#!/usr/bin/env node
import cli from 'commander'
import retrieve from './commands/retrieve.js'
import api from './commands/api.js'
import process from './commands/process.js'
import dotenv from 'dotenv'

dotenv.config()

cli.description("Last.fm Wrapper")
cli.name("wrapper")
cli.addHelpCommand(false)
cli.helpOption(true)

cli.command("retrieve")
.argument("username", "The username for which to download data.")
.option("-b, --basic", "Download only basic user-data")
.option("-f, --full", "Download listening data for previous years.")
.action(retrieve)

cli.command('process')
.argument('username', "")
.option("-t, --tracks", "")
.option("-al, --albums", "")
.option("-ar, --artists", "")
.action(process)

cli.command("api")
.argument("username", "")
.argument("method", "")
.argument("args", "")
.action(api)

// must ALWAYS be at the bottom
cli.parse(process.argv)